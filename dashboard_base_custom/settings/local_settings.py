INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'bootstrap3',
    'django_extensions',

    'dashboard_base_custom',

    'dashboard_base',
    'dashboard_bootstrap3',

    'groups_manager',

]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend',
)

ROOT_URLCONF = "dashboard_base_custom.local_urls"

GROUPS_MANAGER = {
    "AUTH_MODELS_SYNC": True
}
