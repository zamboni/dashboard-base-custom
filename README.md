# Dashboard with Bootstrap3


Customizations for dashboard-base.

## Requirements

To use it, add to installed apps:

```static
INSTALLED_APPS += [
    "dashboard_base_custom",
]
```
